
const express = require('express')
const app = express()
const port = 9999
var bodyParser = require('body-parser');
var mustache = require('mustache-express');

var options = {
    root: __dirname + '/files/',
    dotfiles: 'deny',
    headers: {
        'x-timestamp': Date.now(),
        'x-sent': true
    }
};

app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.engine('html', mustache()) // for rendering 
app.set('view engine', 'html') 


app.all('*', (req, res, next) => { console.log(`${req.method} ${req.url}`); next() })
app.get('/', (req, res) => res.sendFile('index.html', options))
app.get('/style.css', (req, res) => res.sendFile('style.css', options))
app.post('/',  (req, res) => res.render('response.html', {body: JSON.stringify(req.body, null, 2)}))
app.listen(port, () => console.log(`Example app listening on port ${port}!`))


